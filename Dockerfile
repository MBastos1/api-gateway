FROM openjdk:11-jdk-oraclelinux8
LABEL maintainer="pucminas.com"

VOLUME /tmp
EXPOSE 8180

ARG JAR_FILE=target/api-gateway*.jar
ADD ${JAR_FILE} api-gateway.jar

ENTRYPOINT ["sh", "-c", "java -jar /api-gateway.jar"]

