# GISA - API Gateway

## Descrição
API de gerenciamento de API , ponto único de entrada, fica entre o cliente e uma coleção de serviços de back-end. Um API Gateway atua como um proxy reverso para aceitar todas as chamadas de interface de programação de aplicativos (API), agregar vários serviços necessários para atendêlos e retornar o resultado apropriado.

A maioria das APIs corporativas são implantadas por meio de gateways de API. É comum que os gateways de API lidem com tarefas comuns usadas em um sistema de serviços de API, como autenticação de usuário, limitação de taxa e estatísticas.

Todas as requisições feitas pelo frontend do GISA devem passar pelo API Gateway. 

## Instalação



